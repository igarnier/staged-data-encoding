(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2020 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Binary_error

let raise error = raise (Write_error error)

open Binary_writer_state
open Atom_binary_writer

type 'a encoder = state Trx.code -> 'a Trx.code -> unit Trx.code

let dyn_to_static : (state -> 'a -> unit) Trx.code -> 'a encoder =
  fun f state code ->
    .< .~f .~state .~code >.

let static_to_dyn : 'a encoder -> (state -> 'a -> unit) Trx.code =
  fun f ->
    .<fun state value -> .~(f .<state>. .<value>.) >.

type ex_code = Ex : 'a Encoding.t * 'a encoder -> ex_code

type binders = ex_code list

let rec find_fix : type a. binders -> a Encoding.t -> a encoder option =
  fun binders encoding ->
  match binders with
  | [] -> None
  | (Ex (enc, code)) :: tl ->
    (match Encoding.phys_eq_dec enc encoding with
     | None -> find_fix tl encoding
     | Some Eq ->
       Some code)

(** Main recursive writing function. *)
let rec write_rec : type a. binders -> a Encoding.t -> a encoder =
 fun binders e ->
  match find_fix binders e with
  | Some code -> code
  | None -> begin
  let open Encoding in
  match e.encoding with
  | Null ->
    fun _state _value -> .<()>.
  | Empty ->
    fun _state _value -> .<()>.
  | Constant _ ->
    fun _state _value -> .<()>.
  | Ignore ->
    fun _state _value -> .<()>.
  | Bool ->
    dyn_to_static .<Atom.bool>.
  | Int8 ->
    dyn_to_static .<Atom.int8>.
  | Uint8 ->
    dyn_to_static .<Atom.uint8>.
  | Int16 ->
    dyn_to_static .<Atom.int16>.
  | Uint16 ->
    dyn_to_static .<Atom.uint16>.
  | Int31 ->
    dyn_to_static .<Atom.int31>.
  | Int32 ->
    dyn_to_static .<Atom.int32>.
  | Int64 ->
    dyn_to_static .<Atom.int64>.
  | N ->
    dyn_to_static .<Atom.n>.
  | Z ->
    dyn_to_static .<Atom.z>.
  | Float ->
    dyn_to_static .<Atom.float>.
  | Bytes (`Fixed n) ->
    dyn_to_static .<Atom.fixed_kind_bytes n>.
  | Bytes `Variable ->
    fun state value ->
      .<let length = Bytes.length .~value in
        Atom.fixed_kind_bytes length .~state .~value>.
  | String (`Fixed n) ->
    dyn_to_static .<Atom.fixed_kind_string n>.
  | String `Variable ->
    fun state value ->
      .<let length = String.length .~value in
        Atom.fixed_kind_string length .~state .~value>.
  | Padded (e, n) ->
    fun state value ->
      .< .~(write_rec binders e state value) ;
         Atom.fixed_kind_string n .~state (String.make n '\000') >.
  | RangedInt {minimum; maximum} ->
      dyn_to_static .<Atom.ranged_int ~minimum ~maximum>.
  | RangedFloat {minimum; maximum} ->
      dyn_to_static .<Atom.ranged_float ~minimum ~maximum>.
  | String_enum (lookup, _inverse_lookup_code, size) ->
      dyn_to_static .<Atom.string_enum .~lookup size>.
  | Array (length_opt, e) ->
    fun state value ->
    .<
    .~(Codelib.make_match .<length_opt>. @@
       [ .<(function (Some max_length) when Array.length .~value > max_length ->
             raise Array_too_long
       | _ ->
          Array.iter (.~(static_to_dyn (write_rec binders e)) .~state) .~value)>.  [@metaocaml.functionliteral] ])
      >.
  | List (length_opt, e) ->
    fun state value ->
    .<
    .~(Codelib.make_match .<length_opt>. @@
       [ .<(function (Some max_length) when List.length .~value > max_length ->
             raise Array_too_long
       | _ ->
          List.iter (.~(static_to_dyn (write_rec binders e)) .~state) .~value)>.  [@metaocaml.functionliteral] ])
      >.
  | Obj (Req {encoding = e; _}) ->
    write_rec binders e
  | Obj (Opt {kind = `Dynamic; encoding = e; _}) -> (
      fun state value ->
      .<match .~value with
        | None ->
          Atom.bool .~state false
        | Some value ->
          Atom.bool .~state true ;
          .~(write_rec binders e state .<value>.)
      >.)
  | Obj (Opt {kind = `Variable; encoding = e; _}) ->
    fun state value ->
      .<match .~value with
        | None -> ()
        | Some value ->
          .~(write_rec binders e state .<value>.)>.
  | Obj (Dft {encoding = e; _}) ->
      write_rec binders e
  | Objs {left; right; _} ->
    fun state value ->
    .<let (v1, v2) = .~value in
      .~(write_rec binders left state .<v1>.) ;
      .~(write_rec binders right state .<v2>.)>.
  | Tup e ->
    write_rec binders e
  | Tups {left; right; _} ->
    fun state value ->
    .<let (v1, v2) = .~value in
      .~(write_rec binders left state .<v1>.) ;
      .~(write_rec binders right state .<v2>.)
      >.
  | Conv {encoding = e; proj; _} ->
    fun state value ->
    write_rec binders e state .<(.~proj .~value)>.
  | Union {tag_size; cases; _} ->
    let cont : type a. state Trx.code -> int -> a Encoding.t -> a Trx.code -> unit Trx.code =
      fun state tag e v ->
      .<Atom.tag .~(Binary_size.code_of_tag_size tag_size) .~state tag ;
        .~(write_rec binders e state v)>.
    in
    let cases state =
      List.fold_right (fun case acc ->
          match case with
          | (Case { tag = Json_only; _ }) -> acc
          | (Case { encoding; proj; tag = Tag tag; _ }) ->
            (proj (cont state tag encoding)) :: acc
        ) cases []
    in
    fun state value ->
    .<
      .~(Codelib.make_match value (cases state))>.
  | Dynamic_size {kind; encoding = e} ->
    let size_of_kind = Binary_size.integer_to_size kind in
    let kind_expr = Binary_size.code_of_unsigned_integer kind in
    fun state value ->
    .<let initial_offset = .~state.offset in
      Atom.int .~kind_expr .~state 0 ;
      (* place holder for [size] *)
      .~(write_with_limit binders (Binary_size.max_int kind) e state value) ;
      (* patch the written [size] *)
      Atom.set_int
        .~kind_expr
        .~state.buffer
        initial_offset
        (.~state.offset - initial_offset - size_of_kind)>.
  | Check_size {limit; encoding = e} ->
      write_with_limit binders limit e
  | Describe {encoding = e; _} ->
      write_rec binders e
  | Mu {fix; _} ->
    fun state value ->
    .<let rec f = fun state value -> .~(write_rec ((Ex (e, dyn_to_static .<f>.)) :: binders) (fix e) .<state>. .<value>.) in f .~state .~value>.
  | Delayed f ->
      write_rec binders (f ())
end

and write_with_limit : type a. binders -> int -> a Encoding.t -> a encoder =
 fun binders limit e state value ->
 .<
  (* backup the current limit *)
  let old_limit = .~state.allowed_bytes in
  (* install the new limit (only if smaller than the current limit) *)
  let limit =
    match .~state.allowed_bytes with
    | None ->
        limit
    | Some old_limit ->
        min old_limit limit
  in
  .~state.allowed_bytes <- Some limit ;
  .~(write_rec binders e state value) ;
  (* restore the previous limit (minus the read bytes) *)
  match old_limit with
  | None ->
      .~state.allowed_bytes <- None
  | Some old_limit ->
      let remaining =
        match .~state.allowed_bytes with None -> assert false | Some len -> len
      in
      let read = limit - remaining in
      .~state.allowed_bytes <- Some (old_limit - read)>.
