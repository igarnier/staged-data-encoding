(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2020 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Binary_error

type 'a sizer = 'a Trx.code -> int Trx.code

let dyn_to_static : ('a -> int) Trx.code -> 'a sizer =
  fun f code ->
    .< .~f .~code >.

let static_to_dyn : 'a sizer -> ('a -> int) Trx.code =
  fun f ->
    .<fun value -> .~(f .<value>.) >.

type ex_code = Ex : 'a Encoding.t * 'a sizer -> ex_code

type binders = ex_code list

let rec find_fix : type a. binders -> a Encoding.t -> a sizer option =
  fun binders encoding ->
  match binders with
  | [] -> None
  | (Ex (enc, code)) :: tl ->
    (match Encoding.phys_eq_dec enc encoding with
     | None -> find_fix tl encoding
     | Some Eq ->
       Some code)

let n_length value =
  let bits = Z.numbits value in
  if bits = 0 then 1 else (bits + 6) / 7

let z_length value = (Z.numbits value + 1 + 6) / 7

let rec length : type a. binders -> a Encoding.t -> a sizer =
 fun binders e ->
  match find_fix binders e with
  | Some code -> code
  | None -> begin
  let open Encoding in
  match e.encoding with
  (* Fixed *)
  | Null ->
    fun _value -> .< 0 >.
  | Empty ->
    fun _value -> .<0 >.
  | Constant _ ->
    fun _value -> .<0 >.
  | Bool ->
    fun _value -> .<Binary_size.bool >.
  | Int8 ->
    fun _value -> .<Binary_size.int8 >.
  | Uint8 ->
    fun _value -> .<Binary_size.uint8>.
  | Int16 ->
    fun _value -> .<Binary_size.int16>.
  | Uint16 ->
    fun _value -> .<Binary_size.uint16>.
  | Int31 ->
    fun _value -> .<Binary_size.int31>.
  | Int32 ->
    fun _value -> .<Binary_size.int32>.
  | Int64 ->
    fun _value -> .<Binary_size.int64>.
  | N ->
    dyn_to_static .<n_length>.
  | Z ->
    dyn_to_static .<z_length>.
  | RangedInt {minimum; maximum} ->
    let size = Binary_size.integer_to_size
      @@ Binary_size.range_to_size ~minimum ~maximum in
    fun _value -> .<size>.
  | Float ->
      fun _value -> .<Binary_size.float>.
  | RangedFloat _ ->
     fun _value -> .<Binary_size.float>.
  | Bytes (`Fixed n) ->
    fun _value -> .<n>.
  | String (`Fixed n) ->
     fun _value -> .<n>.
  | Padded (e, n) ->
     fun value ->  .<.~(length binders e value) + n>.
  | String_enum (_, _, size) ->
      let size = Binary_size.integer_to_size @@ Binary_size.unsigned_range_to_size size in
      fun _value -> .<size>.
  | Objs {kind = `Fixed n; _} ->
      fun _value -> .<n>.
  | Tups {kind = `Fixed n; _} ->
      fun _value -> .<n>.
  | Union {kind = `Fixed n; _} ->
      fun _value -> .<n>.
  (* Dynamic *)
  | Objs {kind = `Dynamic; left; right} ->
    fun value ->
      .<let (v1, v2) = .~value in
        .~(length binders left .<v1>.) + .~(length binders right .<v2>.) >.
  | Tups {kind = `Dynamic; left; right} ->
    fun value ->
      .<let (v1, v2) = .~value in
        .~(length binders left .<v1>.) + .~(length binders right .<v2>.) >.
  | Union {kind = `Dynamic; tag_size; cases} ->
    let sz = Binary_size.tag_size tag_size in
    let cont e v =
      .<sz + .~(length binders e v)>. in
    let cases =
      List.map (fun (Case { encoding; proj; _ }) ->
          proj (cont encoding)
        ) cases
    in
    fun value ->
    .<.~(Codelib.make_match value cases)>.
  | Mu {kind = `Dynamic; fix; _} ->
    fun value ->
    .<let rec f = fun value -> .~(length ((Ex (e, dyn_to_static .<f>.)) :: binders) (fix e) .<value>.) in f .~value>.
  | Obj (Opt {kind = `Dynamic; encoding = e; _}) ->
    fun value ->
    .<match .~value with None -> 1 | Some value -> 1 + .~(length binders e .<value>.)>.
  (* Variable *)
  | Ignore ->
    fun _value -> .<0>.
  | Bytes `Variable ->
      dyn_to_static .<Bytes.length>.
  | String `Variable ->
      dyn_to_static .<String.length>.
  | Array (length_opt, e) ->
    fun value ->
      .<.~(Codelib.make_match .<length_opt>. @@
        [ .<(function (Some max_length) when Array.length .~value > max_length ->
              raise (Write_error Array_too_long)
        | _ ->
          Array.fold_left (fun acc v -> .~(length binders e .<v>.) + acc) 0 .~value)>.  [@metaocaml.functionliteral] ])
      >.
  | List (length_opt, e) ->
    fun value ->
      .<.~(Codelib.make_match .<length_opt>. @@
        [ .<(function (Some max_length) when List.length .~value > max_length ->
              raise (Write_error List_too_long)
        | _ ->
          List.fold_left (fun acc v -> .~(length binders e .<v>.) + acc) 0 .~value)>.  [@metaocaml.functionliteral] ])
      >.
  | Objs {kind = `Variable; left; right} ->
    fun value ->
      .<let (v1, v2) = .~value in
        .~(length binders left .<v1>.) + .~(length binders right .<v2>.) >.
  | Tups {kind = `Variable; left; right} ->
    fun value ->
      .<let (v1, v2) = .~value in
        .~(length binders left .<v1>.) + .~(length binders right .<v2>.) >.
  | Obj (Opt {kind = `Variable; encoding = e; _}) ->
    fun value ->
    .<match .~value with None -> 0 | Some value -> .~(length binders e .<value>.) >.
  | Union {kind = `Variable; tag_size; cases} ->
    let cont e v =
      .<Binary_size.tag_size tag_size + .~(length binders e v)>. in
    let cases =
      List.map (fun (Case { encoding; proj; _ }) ->
          proj (cont encoding)
        ) cases
    in
    fun value ->
    .<.~(Codelib.make_match value cases)>.
  | Mu {kind = `Variable; fix; _} ->
    fun value ->
    .<let rec f = fun value -> .~(length ((Ex (e, dyn_to_static .<f>.)) :: binders) (fix e) .<value>.) in f .~value>.
  (* Recursive*)
  | Obj (Req {encoding = e; _}) ->
    length binders e
  | Obj (Dft {encoding = e; _}) ->
    length binders e
  | Tup e ->
    length binders e
  | Conv {encoding = e; proj; _} ->
    fun value ->
      length binders e .<(.~proj .~value)>.
  | Describe {encoding = e; _} ->
    length binders e
  | Dynamic_size {kind; encoding = e} ->
    let size = Binary_size.integer_to_size kind in
    fun value ->
      .<
      let length = .~(length binders e value) in
      size + length
      >.
  | Check_size {limit; encoding = e} ->
    fun value ->
      .<let length = .~(length binders e value) in
        if length > limit then raise (Write_error Size_limit_exceeded) ;
        length >.
  | Delayed f ->
    length binders (f ())
end

let fixed_length e =
  match Encoding.classify e with
  | `Fixed n ->
      Some n
  | `Dynamic | `Variable ->
      None

let fixed_length_exn e =
  match fixed_length e with
  | Some n ->
      n
  | None ->
      invalid_arg "Data_encoding.Binary.fixed_length_exn"
