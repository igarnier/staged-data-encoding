(* Examples of metaprogramming encoders and decoders with metaocaml *)
(* See http://okmij.org/ftp/ML/MetaOCaml.html to learn more about metaocaml. *)

open Staged_data_encoding

(* ------------------------------------------------------------------------- *)

(* An encoding for the bintree type,
   type t =
   | Leaf of int
   | Node of t * t  *)

let bintree_encoding =
  let open Encoding in
  let case1 =
    case
      (Tag 0)
      int31
      ~title:"Leaf"
      { proj = (fun k -> .<function (Bintree.Leaf i) -> .~(k .<i>.)>.  [@ocaml.warning "-8"] [@metaocaml.functionliteral]) }
      { inj = (fun k -> .<function i -> .~(k .<(Bintree.Leaf i)>.)>.  [@ocaml.warning "-8"] [@metaocaml.functionliteral]) }
  in
  let case2 fix =
    case
      (Tag 1)
      (dynamic_size (tup2 (dynamic_size fix) fix))
      ~title:"Node"
      { proj = (fun k -> .<function (Bintree.Node (l, r)) -> .~(k .<(l, r)>.)>. [@ocaml.warning "-8"] [@metaocaml.functionliteral]) }
      { inj = (fun k -> .<function (l, r) -> .~(k .<(Bintree.Node (l, r))>.)>. [@ocaml.warning "-8"] [@metaocaml.functionliteral]) }
  in
  mu "bintree" (fun fix -> Encoding.union [ case1; case2 fix ])

let bintree_compute_size =
  Binary_length.static_to_dyn
    (Binary_length.length [] bintree_encoding)

let () =
  Format.printf "bintree binary length:@." ;
  Format.printf "%a"
    Codelib.print_code
    bintree_compute_size

let bintree_encoder =
  Binary_writer.static_to_dyn
    (Binary_writer.write_rec [] bintree_encoding)

let () =
  Format.printf "bintree encoder:@." ;
  Format.printf "%a"
    Codelib.print_code
    bintree_encoder


(* ------------------------------------------------------------------------- *)
(* An encoding for micheline *)

let canonical_location_encoding =
  let open Encoding in
  def
    "micheline.location"
    ~title:"Canonical location in a Micheline expression"
    ~description:
      "The location of a node in a Micheline expression tree in prefix order, \
       with zero being the root and adding one for every basic node, sequence \
       and primitive application."
  @@ int31

let internal_canonical_encoding ~semantics ~variant prim_encoding =
  let open Encoding in
  let open Micheline in
  let int_encoding = obj1 (req "int" z) in
  let string_encoding = obj1 (req "string" string) in
  let bytes_encoding = obj1 (req "bytes" bytes) in
  let int_encoding tag =
    case
      tag
      int_encoding
      ~title:"Int"
      { proj = (fun k -> .<function Int (_, v) -> .~(k .<v>.)>.
                                                  [@ocaml.warning "-8"]
                                                  [@metaocaml.functionliteral])  }
      { inj = (fun k -> .<fun v -> .~(k .<Int (0, v)>.)>.
                                   [@metaocaml.functionliteral]) }
  in
  let string_encoding tag =
    case
      tag
      string_encoding
      ~title:"String"
      { proj = (fun k -> .<function String (_, v) -> .~(k .<v>.)>.
                                                     [@ocaml.warning "-8"]
                                                     [@metaocaml.functionliteral]) }
      { inj = (fun k -> .<fun v -> .~(k .<String (0, v)>.)>.
                                   [@metaocaml.functionliteral]) }
  in
  let bytes_encoding tag =
    case
      tag
      bytes_encoding
      ~title:"Bytes"
      { proj = (fun k -> .<function Bytes (_, v) -> .~(k .<v>.)>.
                                                    [@ocaml.warning "-8"]
                                                    [@metaocaml.functionliteral]) }
      { inj = (fun k -> .<fun v -> .~(k .<Bytes (0, v)>.)>.
                                   [@metaocaml.functionliteral]) }
  in
  let seq_encoding tag expr_encoding =
    case
      tag
      (list expr_encoding)
      ~title:"Sequence"
      { proj = (fun k -> .<function Seq (_, v) -> .~(k .<v>.)>.
                                                  [@ocaml.warning "-8"]
                                                  [@metaocaml.functionliteral]) }
      { inj = (fun k -> .<fun args -> .~(k .<Seq (0, args)>.)>.
                                      [@metaocaml.functionliteral]) }
  in
  let annots_encoding =
    let split =
      .<fun s ->
        if s = "" && semantics <> V0 then []
        else
          let annots = String.split_on_char ' ' s in
          List.iter
            (fun a ->
               if String.length a > 255 then failwith "Oversized annotation")
            annots ;
          if String.concat " " annots <> s then
            failwith
              "Invalid annotation string, must be a sequence of valid \
               annotations with spaces" ;
          annots>.
    in
    (conv .<(String.concat " ")>. split string)
  in
  let application_encoding tag expr_encoding =
    case
      tag
      ~title:"Generic prim (any number of args with or without annot)"
      (obj3
         (req "prim" prim_encoding)
         (dft "args" (list expr_encoding) [])
         (dft "annots" annots_encoding []))
      { proj = (fun k ->
            .<function
              | Prim (_, prim, args, annots) -> .~(k .<(prim, args, annots)>.)>.
                                                [@ocaml.warning "-8"]
                                                [@metaocaml.functionliteral]) }
      { inj = (fun k ->
            .<fun (prim, args, annots) -> .~(k .<Prim (0, prim, args, annots)>.)>.
                                          [@metaocaml.functionliteral]) }
  in
  let node_encoding =
    mu
      ("micheline." ^ variant ^ ".expression")
      (fun expr_encoding ->
         (union
            ~tag_size:`Uint8
            [ int_encoding (Tag 0);
              string_encoding (Tag 1);
              seq_encoding (Tag 2) expr_encoding;
              (* No args, no annot *)
              case
                (Tag 3)
                ~title:"Prim (no args, annot)"
                (obj1 (req "prim" prim_encoding))
                { proj = (fun k ->
                      .<function Prim (_, v, [], []) -> .~(k .<v>.)>.
                                                        [@ocaml.warning "-8"]
                                                        [@metaocaml.functionliteral]) }
                { inj = (fun k -> .<fun v -> .~(k .<Prim (0, v, [], [])>.)>.
                                             [@metaocaml.functionliteral]) } ;
              (* No args, with annots *)
              case
                (Tag 4)
                ~title:"Prim (no args + annot)"
                (obj2
                   (req "prim" prim_encoding)
                   (req "annots" annots_encoding))
                { proj = (fun k ->
                      .<function
                        | Prim (_, v, [], annots) -> .~(k .<(v, annots)>.) >.
                                                     [@ocaml.warning "-8"]
                                                     [@metaocaml.functionliteral]) }
                { inj = (fun k ->
                      .<function (prim, annots) -> .~(k .<Prim (0, prim, [], annots)>.)>.
                                                   [@metaocaml.functionliteral]) } ;
              (* Single arg, no annot *)
              case
                (Tag 5)
                ~title:"Prim (1 arg, no annot)"
                (obj2 (req "prim" prim_encoding) (req "arg" expr_encoding))
                { proj = (fun k ->
                      .<function
                        | Prim (_, v, [arg], []) -> .~(k .<(v, arg)>.)>.
                                                     [@ocaml.warning "-8"]
                                                     [@metaocaml.functionliteral]) }
                { inj = (fun k -> .<function (prim, arg) -> .~(k .<Prim (0, prim, [arg], [])>.)>.
                                                            [@metaocaml.functionliteral]) };
              (* Single arg, with annot *)
              case
                (Tag 6)
                ~title:"Prim (1 arg + annot)"
                (obj3
                   (req "prim" prim_encoding)
                   (req "arg" expr_encoding)
                   (req "annots" annots_encoding))
                { proj = (fun k -> .<function
                  | Prim (_, prim, [arg], annots) -> .~(k .<(prim, arg, annots)>.) >.
                                                     [@ocaml.warning "-8"]
                                                     [@metaocaml.functionliteral]) }
                { inj = (fun k ->
                      .<fun (prim, arg, annots) -> .~(k .<Prim (0, prim, [arg], annots)>.)>.
                                                   [@metaocaml.functionliteral]) };
              (* Two args, no annot *)
              case
                (Tag 7)
                ~title:"Prim (2 args, no annot)"
                (obj3
                   (req "prim" prim_encoding)
                   (req "arg1" expr_encoding)
                   (req "arg2" expr_encoding))
                { proj = (fun k -> .<function
                      | Prim (_, prim, [arg1; arg2], []) -> .~(k .<(prim, arg1, arg2)>.)>.
                                                            [@ocaml.warning "-8"]
                                                            [@metaocaml.functionliteral]) }
                { inj = (fun k -> .<fun (prim, arg1, arg2) -> .~(k .<Prim (0, prim, [arg1; arg2], [])>.)>.
                                                              [@metaocaml.functionliteral]) };
              (* Two args, with annots *)
              case
                (Tag 8)
                ~title:"Prim (2 args + annot)"
                (obj4
                   (req "prim" prim_encoding)
                   (req "arg1" expr_encoding)
                   (req "arg2" expr_encoding)
                   (req "annots" annots_encoding))
                { proj = (fun k -> .<function
                  | Prim (_, prim, [arg1; arg2], annots) -> .~(k .<(prim, arg1, arg2, annots)>.) >.
                                                            [@ocaml.warning "-8"]
                                                            [@metaocaml.functionliteral]) }
                { inj = (fun k -> .<fun (prim, arg1, arg2, annots) ->
                      .~(k .<Prim (0, prim, [arg1; arg2], annots)>.) >.
                      [@metaocaml.functionliteral]) };
              (* General case *)
              application_encoding (Tag 9) expr_encoding;
              bytes_encoding (Tag 10) ]))
  in
  conv
    .<(function Canonical node -> node)>.
    .<(fun node -> strip_locations node)>.
    node_encoding

let canonical_encoding_v1 ~variant prim_encoding =
  internal_canonical_encoding ~semantics:V1 ~variant prim_encoding

(* ------------------------------------------------------------------------- *)
(* An encoding for Michelson primitives *)
(* The strings are actually unused in binary mode. *)

let prim_encoding =
  let open Staged_data_encoding.Encoding in
  let open Michelson_primitives in
  def "michelson.v1.primitives"
  @@ string_enum
     [ (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("parameter", K_parameter)>.;
         .<("storage", K_storage)>.;
         .<("code", K_code)>.;
         .<("False", D_False)>.;
         .<("Elt", D_Elt)>.;
         .<("Left", D_Left)>.;
         .<("None", D_None)>.;
         .<("Pair", D_Pair)>.;
         .<("Right", D_Right)>.;
         .<("Some", D_Some)>.;
         (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("True", D_True)>.;
         .<("Unit", D_Unit)>.;
         .<("PACK", I_PACK)>.;
         .<("UNPACK", I_UNPACK)>.;
         .<("BLAKE2B", I_BLAKE2B)>.;
         .<("SHA256", I_SHA256)>.;
         .<("SHA512", I_SHA512)>.;
         .<("ABS", I_ABS)>.;
         .<("ADD", I_ADD)>.;
         .<("AMOUNT", I_AMOUNT)>.;
         (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("AND", I_AND)>.;
         .<("BALANCE", I_BALANCE)>.;
         .<("CAR", I_CAR)>.;
         .<("CDR", I_CDR)>.;
         .<("CHECK_SIGNATURE", I_CHECK_SIGNATURE)>.;
         .<("COMPARE", I_COMPARE)>.;
         .<("CONCAT", I_CONCAT)>.;
         .<("CONS", I_CONS)>.;
         .<("CREATE_ACCOUNT", I_CREATE_ACCOUNT)>.;
         .<("CREATE_CONTRACT", I_CREATE_CONTRACT)>.;
         (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("IMPLICIT_ACCOUNT", I_IMPLICIT_ACCOUNT)>.;
         .<("DIP", I_DIP)>.;
         .<("DROP", I_DROP)>.;
         .<("DUP", I_DUP)>.;
         .<("EDIV", I_EDIV)>.;
         .<("EMPTY_MAP", I_EMPTY_MAP)>.;
         .<("EMPTY_SET", I_EMPTY_SET)>.;
         .<("EQ", I_EQ)>.;
         .<("EXEC", I_EXEC)>.;
         .<("FAILWITH", I_FAILWITH)>.;
         (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("GE", I_GE)>.;
         .<("GET", I_GET)>.;
         .<("GT", I_GT)>.;
         .<("HASH_KEY", I_HASH_KEY)>.;
         .<("IF", I_IF)>.;
         .<("IF_CONS", I_IF_CONS)>.;
         .<("IF_LEFT", I_IF_LEFT)>.;
         .<("IF_NONE", I_IF_NONE)>.;
         .<("INT", I_INT)>.;
         .<("LAMBDA", I_LAMBDA)>.;
         (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("LE", I_LE)>.;
         .<("LEFT", I_LEFT)>.;
         .<("LOOP", I_LOOP)>.;
         .<("LSL", I_LSL)>.;
         .<("LSR", I_LSR)>.;
         .<("LT", I_LT)>.;
         .<("MAP", I_MAP)>.;
         .<("MEM", I_MEM)>.;
         .<("MUL", I_MUL)>.;
         .<("NEG", I_NEG)>.;
         (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("NEQ", I_NEQ)>.;
         .<("NIL", I_NIL)>.;
         .<("NONE", I_NONE)>.;
         .<("NOT", I_NOT)>.;
         .<("NOW", I_NOW)>.;
         .<("OR", I_OR)>.;
         .<("PAIR", I_PAIR)>.;
         .<("PUSH", I_PUSH)>.;
         .<("RIGHT", I_RIGHT)>.;
         .<("SIZE", I_SIZE)>.;
         (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("SOME", I_SOME)>.;
         .<("SOURCE", I_SOURCE)>.;
         .<("SENDER", I_SENDER)>.;
         .<("SELF", I_SELF)>.;
         .<("STEPS_TO_QUOTA", I_STEPS_TO_QUOTA)>.;
         .<("SUB", I_SUB)>.;
         .<("SWAP", I_SWAP)>.;
         .<("TRANSFER_TOKENS", I_TRANSFER_TOKENS)>.;
         .<("SET_DELEGATE", I_SET_DELEGATE)>.;
         .<("UNIT", I_UNIT)>.;
         (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("UPDATE", I_UPDATE)>.;
         .<("XOR", I_XOR)>.;
         .<("ITER", I_ITER)>.;
         .<("LOOP_LEFT", I_LOOP_LEFT)>.;
         .<("ADDRESS", I_ADDRESS)>.;
         .<("CONTRACT", I_CONTRACT)>.;
         .<("ISNAT", I_ISNAT)>.;
         .<("CAST", I_CAST)>.;
         .<("RENAME", I_RENAME)>.;
         .<("bool", T_bool)>.;
         (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("contract", T_contract)>.;
         .<("int", T_int)>.;
         .<("key", T_key)>.;
         .<("key_hash", T_key_hash)>.;
         .<("lambda", T_lambda)>.;
         .<("list", T_list)>.;
         .<("map", T_map)>.;
         .<("big_map", T_big_map)>.;
         .<("nat", T_nat)>.;
         .<("option", T_option)>.;
         (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("or", T_or)>.;
         .<("pair", T_pair)>.;
         .<("set", T_set)>.;
         .<("signature", T_signature)>.;
         .<("string", T_string)>.;
         .<("bytes", T_bytes)>.;
         .<("mutez", T_mutez)>.;
         .<("timestamp", T_timestamp)>.;
         .<("unit", T_unit)>.;
         .<("operation", T_operation)>.;
         (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
         .<("address", T_address)>.;
         (* Alpha_002 addition *)
         .<("SLICE", I_SLICE)>.;
         (* Alpha_005 addition *)
         .<("DIG", I_DIG)>.;
         .<("DUG", I_DUG)>.;
         .<("EMPTY_BIG_MAP", I_EMPTY_BIG_MAP)>.;
         .<("APPLY", I_APPLY)>.;
         .<("chain_id", T_chain_id)>.;
         .<("CHAIN_ID", I_CHAIN_ID)>.
         (* New instructions must be added here, for backward compatibility of the encoding. *)
        ]

let prim_compute_size =
  Binary_length.static_to_dyn
    (Binary_length.length [] prim_encoding)

let prim_encoder =
  Binary_writer.static_to_dyn
    (Binary_writer.write_rec [] prim_encoding)

let () =
  Format.printf "------------------------------------------------------------@." ;
  Format.printf "prim binary length:@." ;
  Format.printf "%a"
    Codelib.print_code
    prim_compute_size

let () =
  Format.printf "------------------------------------------------------------@." ;
  Format.printf "prim binary encoder:@." ;
  Format.printf "%a"
    Codelib.print_code
    prim_encoder
(* ------------------------------------------------------------------------- *)

let expr_encoding =
  canonical_encoding_v1
    ~variant:"michelson_v1"
    prim_encoding

let expr_compute_size =
  Binary_length.static_to_dyn
    (Binary_length.length [] expr_encoding)

let expr_encoder =
  Binary_writer.static_to_dyn
    (Binary_writer.write_rec [] expr_encoding)

let () =
  Format.printf "------------------------------------------------------------@." ;
  Format.printf "Michelson binary length:@." ;
  Format.printf "%a"
    Codelib.print_code
    expr_compute_size

let () =
  Format.printf "------------------------------------------------------------@." ;
  Format.printf "Michelson binary encoder:@." ;
  Format.printf "%a"
    Codelib.print_code
    expr_encoder
