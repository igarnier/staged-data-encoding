type t =
  | Leaf of int
  | Node of t * t
